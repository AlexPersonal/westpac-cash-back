(function ($) {
    $(document).ready(function () {
        var calculateValue = window.calculateValue,
        updateView = function (args) {
			$('#currentBalance input').blur();
			$('#interestRate input').blur();
            args = args || {};
            args.update = args.update || false;
            var balanceVal, interestVal;
            if (args) {
                switch (args.targ) {
                    case "balance":
                        balanceVal = args.value;
                        break;
                    case "interest":
                        interestVal = args.value;
                        break;
                }
            }
            balanceVal = balanceVal || $bSlider.slider("value");
            interestVal = interestVal || $ciSlider.slider("value");

            if ($ciInput.val() == interestVal && $bInput.val() == balanceVal && !args.update) return; //nothing changed; end function;
            if ($ciInput.val() != interestVal || args.update) {
                //rotate balance dial                
                var opts = $ciSlider.slider("option");
                // var rotation = ((interestVal - opts.min) / (opts.max - opts.min)) * 180;
                // $ciDial.stop().animate({ rotate: rotation }, 1300, "easeInOutExpo");
                // if ($html.hasClass("lt-ie9")) {
                //     //do ie stuff                    
                //     var rotationClass = Math.round((rotation / 180 * 100) / 10) * 10;
                //     $ciIe.attr("class", "").addClass("ie rotation" + rotationClass);
                // }
            }

            if (accounting.unformat($bInput.val()) != balanceVal || args.update) {
                //rotate interest dial                
                var opts = $bSlider.slider("option");
                // var rotation = ((balanceVal - opts.min) / (opts.max - opts.min)) * 180;
                // $bDial.stop().animate({ rotate: rotation }, 1300, "easeInOutExpo");
                // if ($html.hasClass("lt-ie9")) {
                //     //do ie stuff
                //     var rotationClass = Math.round((rotation / 180 * 100) / 10) * 10;
                //     $bIe.attr("class", "").addClass("ie rotation" + rotationClass);
                // }
            }

            var dg = new DigitCounter();
            var bFrom = { property: parseFloat(accounting.unformat($bInput.val())) }, bTo = { property: balanceVal };
            if (bFrom.property != bTo.property || args.update) {
                $bInput.val(accounting.formatMoney(balanceVal, "", 0, ",", "."));
                $('#balanceValue').text($bInput.val());
                // Taking away the digit animation
                // dg.countTo(bFrom, bTo, $bInput, function () {
                //     //step                
                //     $bInput.val('$'+accounting.formatMoney(this.property, "", 0, ",", "."));
                // }, function () {
                //     //complete
                //     $bInput.val('$'+accounting.formatMoney(balanceVal, "", 0, ",", "."));                    
                // });
            }

            var ciFrom = { property: parseFloat(accounting.unformat($ciInput.val())) }, ciTo = { property: interestVal };
            if (ciFrom.property != ciTo.property || args.update) {
                $ciInput.val(accounting.formatMoney(interestVal, "", 2, ",", ".")+"%");
                // Taking away the digit animation
                // dg.countTo(ciFrom, ciTo, $ciInput, function () {
                //     $ciInput.val(accounting.formatMoney(this.property, "", 2, ",", ".")+"%");
                // }, function () {
                //     $ciInput.val(accounting.formatMoney(interestVal, "", 2, ",", ".")+"%");                    
                // });
            }
            var result = calculateValue(balanceVal, interestVal / 12, $balPeriod.text(), currentInterestValue);
            var from = { property: parseFloat(accounting.unformat($totalValue.text())) }; 
            var to = { property: result };
            if (parseFloat($totalValue.text()) == result) return;

            var options = {
                symbol: "",
                decimal: ".",
                thousand: ",",
                precision: 2,
                format: "%s%v"
            };

            dg.countTo(from, to, $totalValue, function () {
                $totalValue.html(accounting.formatMoney(this.property, options));
            }, function () {
                $totalValue.html(accounting.formatMoney(result, options));
            });
        };
        var $bInput = $('#currentBalance input'), 
            $ciInput = $('#interestRate input');
        var balanceArgs = {
            animate: "fast",
            max: 50000,
            min: 0,
            step: 100,
            value: $bInput.val().replace('$', '').replace(',', ''),
            change: function (evt, ui) {
                updateView({ targ: "balance", value: ui.value, update: false });
            },
            slide: function (evt, ui) {
                updateView({ targ: "balance", value: ui.value, update: false });
            }
        };

        var interestArgs = {
            animate: "fast",
            max: 40,
            min: parseFloat($(".ir").text()),
            step: 0.01,
            value: $ciInput.val().replace('%', ''),
            change: function (evt, ui) {
                updateView({ targ: "interest", value: ui.value, update: false });
				
            },
            slide: function (evt, ui) {
                updateView({ targ: "interest", value: ui.value, update: false });
            }
			
        };
        var $totalValue = $("#totalValue");
        var is = new InitializeSlider();
            is.setup({
                targ: "#currentBalance .slider",
                args: balanceArgs,
                update: updateView,
                input: "#currentBalance input"
            });
            is.setup({
                targ: "#interestRate .slider",
                args: interestArgs,
                update: updateView,
                input: "#interestRate input"
            });
        var $html = $("html"),
            $bSlider = $("#currentBalance .slider").slider(),
            $ciSlider = $("#interestRate .slider").slider(),
            $bIe,
            $ciIe,
            $balPeriod = $(".bal-period").first(),
            currentInterestValue = $(".ir").first().text();

        // if ($html.hasClass("lt-ie9")) {
        //     $bIe = $(".balance .ie");
        //     $ciIe = $(".currentInterestRate .ie");
        // }

        // var ac = new AreaClick();

        // $(".balance .mask").bind("click", function (evt) {
        //     var $this = $(this);
        //     ac.handleClick({
        //         targ: $this,
        //         evt: evt,
        //         maxValue: $bSlider.slider("option").max,
        //         maxDeg: 180,
        //         complete: function (result) {
        //             if (result.clicked) {
        //                 $bSlider.slider("value", result.value);
        //             }
        //         }
        //     });
        // });

        // $(".currentInterestRate .mask").bind("click", function (evt) {
        //     var $this = $(this);
        //     ac.handleClick({
        //         targ: $this,
        //         evt: evt,
        //         maxValue: $ciSlider.slider("option").max,
        //         maxDeg: 180,
        //         complete: function (result) {
        //             if (result.clicked) {
        //                 $ciSlider.slider("value", result.value);
        //             }
        //         }
        //     });
        // });
        updateView({ update: true });
		
		/*----------------------------------------------*/
        /* Additional functions
        /*----------------------------------------------*/

        // Clear the interval when one of the input fields is selected
        $('#currentBalance input, #interestRate input').click(function(){
            clearInterval(reAnimate);
            $(this).val('');
            //console.log("cleared")
        });
		

		var reAnimate = setInterval(function(){
			var startValue = 0;
			$totalValue.html(startValue);
			updateView({ update: true });
		}, 5000);

        

    });
})(window.jQuery);