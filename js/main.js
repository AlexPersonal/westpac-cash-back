// Sticky Footer

if ($(window).width() < '768' || $(document).width() < '768'){
	$('body').addClass('mobile');
}

if ($('.mobile').index() > -1){
	//$('.desktopBtn').hide();
		$('.apply-now').waypoint('sticky', {
		direction: 'up',
		stuckClass: 'stuck',
		offset: 'bottom-in-view',
		wrapper: '<div class="apply-wrapper text-center" />'
	});
	
} else {
	$('.apply-now').hide();
		$('.apply-now').waypoint('sticky', {
		direction: 'up',
		stuckClass: 'stuck',
		offset: 'bottom-in-view',
		wrapper: '<div class="apply-wrapper text-center" />'
	});
		
		$('#section2').waypoint(function(down) {
		  $('.apply-now').show();
		});
		var eTop = $('#apply').offset().top; //get the offset top of the element
		$(window).scroll(function() { //when window is scrolled
		if (eTop - $(window).scrollTop() >= '-140'){
			$('.apply-now').hide();
		}
  		});
	
}	


// URL Paramaters
function getUrlValue(){
    var searchString = window.location.search.substring(1),
        subString,
        sourceBlock;
    if(searchString.indexOf('source=') !== -1) {
        subString = searchString.split('source=');
        sourceBlock = subString[1].split('&');
		console.log(sourceBlock[0]);
        return sourceBlock[0];
		
    } else {
        return sourceBlock;
    }    
}


function removeLogoClasses() {
    $('.aff-logo').removeClass('ccf-logo cw-logo ccc-logo rc-logo ic-logo');
     $('.mobile-ccf-logo', '.mobile-cw-logo', '.mobile-ccc-logo', '.mobile-rc-logo', '.mobile-ic-logo').css('display', "none"); 
}


$(function () {
    var loc = window.location.href,
        qs = loc.substring(loc.indexOf('?')+1, loc.length),
        grab = getUrlValue();
		console.log(['ccf','cw','ccc','rc','ic'].indexOf(grab));
if (['ccf','cw','ccc','rc','ic'].indexOf(grab) > -1){
	console.log('affiliate');
	$('#disclaimer .social').removeClass('noAffiliate').addClass('affiliate');
	$('#partners .social').removeClass('noAffiliate').addClass('affiliate');
} else {
	console.log('no affiliate');
	$('#disclaimer .social').removeClass('affiliate').addClass('noAffiliate');
	$('#partners .social').removeClass('affiliate text-right').addClass('noAffiliate');
}

/* Affiliate logo change css */

switch (grab) {
    case 'ccf':
        removeLogoClasses();
         $('.aff-logo').addClass('ccf-logo');
         $('.mobile-ccf-logo').css('display', "block");
        break;
    case 'cw':
        removeLogoClasses();
         $('.aff-logo').addClass('cw-logo');
        $('.mobile-cw-logo').css('display', "block");
        break;
    case 'ccc':
        removeLogoClasses();
        $('.aff-logo').addClass('ccc-logo');
        $('.mobile-ccc-logo').css('display', "block");
        break;
    case 'rc':
        removeLogoClasses();
         $('.aff-logo').addClass('rc-logo');
        $('.mobile-rc-logo').css('display', "block");
        break;
    case 'ic':
        removeLogoClasses();
         $('.aff-logo').addClass('ic-logo');
         $('.mobile-ic-logo').css('display', "block");
        break;
}


    if(qs.indexOf('chtype=aff') !== -1 || qs.indexOf('medium=aff') !== -1){
        $('body').addClass('no-info');
    } else{
        $('body').removeClass('no-info');
    }

	$('.terms-trigger').on('click', function(){
		$(this).toggleClass('open');
		$('.terms-conditions').toggle();
	});
});


// Carousel enable/disable
if ($(document).width() > '768'){
		$('.carousel').carousel();
		$('.carousel').removeClass('noCarousel');
	} else {
		$('.carousel').addClass('noCarousel');
		console.log('no carousel');
		$('.carousel').carousel('pause');
	}
$( window ).resize(function() {
	if ($(window).width() > '768' || $(document).width() > '768'){
		$('.carousel').carousel();
		$('.carousel').removeClass('noCarousel');
	} else {
		$('.carousel').addClass('noCarousel');
		console.log('no carousel');
		$('.carousel').carousel('pause');
	}
});


