﻿(function (window) {
    var initializeSlider = function () {

    };
    initializeSlider.prototype.setup = function (obj) {
        if (!obj.args.slide) {
            //create slide event handler
            obj.args.slide = function (event, ui) {
                var $this = $(this);

                // if ($.browser.msie) {
                //     var opts = $this.slider("option");
                //     var perc = ((ui.value - opts.min) / (opts.max - opts.min)) * 100;
                //     var c = Math.round(perc / 10) * 10;
                //     $($this.parent()[0]).find(".dial").attr("class", "dial").addClass("per" + c.toString());
                // }

                if (obj.update) {
                    var args = {
                        value: ui.value,
                        className: $($this.parent()[0]).attr("class")
                    };
                    obj.update(args);
                }
            }
        }
        if (!obj.args.change) {
            //create change event handler
            obj.args.change = function (event, ui) {
                var $this = $(this);
                var opts = $this.slider("option");
                // var rotation = ((ui.value - opts.min) / (opts.max - opts.min)) * (obj.maxRotation || 180);
                // $gauge.stop().animate({ rotate: rotation }, 1300, "easeInOutExpo");
                if (obj.update) {
                    var args = {
                        value: ui.value,
                        className: $($this.parent()[0]).attr("class")
                    };
                    obj.update(args);
                }
            }
        }

        if (obj.input) {
            var $input = $(obj.input);
            $input.bind("focus", function (evt) {
                setTimeout(function () {
                    $input.select();
                }, 200);
            });
            var intervalObj;
            $input.bind("keyup", function (evt) {
                if (evt.keyCode == 9) {
                    return;
                }
                var rule = /[+0-9]/; //number only
                clearInterval(intervalObj);
                var self = this;
                if (isNaN(this.value)) {
                    this.value = this.previousValue || this.value;
                    setTimeout(function (evt) {
                        self.select();
                    }, 100);
                } else {
                    intervalObj = setInterval(function () {
                        if ($.fn.slider) {
                            $targ.slider("value", $vi.val());
                        } 
                        self.previousValue = self.value;
                        clearInterval(intervalObj);
                        if (obj.update) {
                            obj.update.call(this, { update: true }); //tight coupling remove this
                        }
                    }, 1000);
                }

            }).bind("keypress", function (evt) {
                var unicode = evt.charCode ? evt.charCode : evt.keyCode,
                    digitLength = this.value.replace(/[^0-9\s]/gi, '').length;
                    
                if(digitLength > parseInt($(this).attr('data-maxDigit'))-1){
                    return false;
                }
                if (unicode != 8 && unicode != 46) { //if the key isn't the backspace key or dot (which we should allow)
                    if (unicode < 48 || unicode > 57) //if not a number
                        return false; //disable key press
                }
                //this.previousValue = this.value;
            });
        }

        var $targ = $(obj.targ);
        $targ.slider(obj.args);
        //initialize sliders

        // var $dial = $targ.parent().find(".dial");
        // var $gauge = $dial.find(".gauge");
        var $vi = $(obj.input);
        $vi.val($targ.slider("value"));
    };
    window.InitializeSlider = initializeSlider;
})(window);