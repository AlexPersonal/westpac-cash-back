//dependencies
// *jQuery
// *accounting.js

/**
    targ: requires to be a selected jquery node.
*/
(function ($, accounting) {
    var digitCounter = function () {

    };
    digitCounter.prototype.countTo = function (from, to, $targ, step, complete, delay, easing) {
        jQuery(from).animate(to, {
            duration: delay || 1100,
            easing: easing || "easeInOutExpo",
            step: step, 
            complete: complete
        });
    }
    window.DigitCounter = digitCounter;
})(window.jQuery, window.accounting);