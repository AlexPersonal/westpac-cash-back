function getUrlValue(varSearch){
    var searchString = window.location.search.substring(1),
        subString,
        promoCodeBlock;
    if(searchString.indexOf('promoCode=') !== -1) {
        subString = searchString.split('promoCode=');
        promoCodeBlock = subString[1].slice(0, 5);
		
        return promoCodeBlock;
    } else {
        return;
    }    
}

(function () {
    var loc = window.location.href,
        qs = loc.substring(loc.indexOf('?')+1, loc.length),
        promoCode = getUrlValue('promoCode');

    if(promoCode) {
        var currentTarget = $('.applyNow a').attr('href');
            hrefSplit = currentTarget.split('promoCode=');

            //console.log(hrefSplit[1]);

            //console.log(hrefSplit)

        $('.applyNow a').each(function(){
            this.href = this.href.replace(hrefSplit[1], getUrlValue('promoCode'));
        });
    }
	
	
    if(qs.indexOf('aff') !== -1){
        $('body').addClass('noContactInfo');
    } else{
        $('body').removeClass('noContactInfo');
    }

    $('footer').on('click', '#btnDisclaimer', function(e){
        e.preventDefault();
        $(e.target).toggleClass('open').parents('footer').find('.disclaimerWrap').slideToggle();
    });

    var calculateValue = function (balance, interest, duration, currentInterestRate) {
        duration = duration || 9;
        var iptInterest = interest;
        var iptBalance = balance;        
        var balTransferInterest = (currentInterestRate / 12) / 100;
        var payment = 0;
        var paymentCounter = 0;
        var resultIncrement1 = 0;
        var resultIncrement2 = 0;
        var interest = 0;
        var interestCompare = 0;
        var interestChanged = 0;
        var interestChangedCounter = 0;
        var counter = iptBalance;
        var counterCompare = iptBalance;
        
        for (i = 0; i < duration; i++) {
            interestChanged = counter * (iptInterest / 100);
            if ((counter + interestChanged) * 0.02 > 10) {
                payment = ((counter + interestChanged) * 0.02);
            } else {
                payment = 10;
            }
            counter = eval((counter + interestChanged - payment).toFixed(2));
            resultIncrement1 += eval(interestChanged.toFixed(2));
            interestChangedCounter = eval((counterCompare * balTransferInterest).toFixed(2));
            if ((counterCompare + interestChangedCounter) * 0.02 > 10) {
                paymentCounter = ((counterCompare + interestChangedCounter) * 0.02);
            } else {
                paymentCounter = 10;
            }
            counterCompare = counterCompare + interestChangedCounter - eval(paymentCounter.toFixed(2));
            resultIncrement2 += eval(interestChangedCounter.toFixed(2));
        }
        var result = (resultIncrement1 - resultIncrement2).toFixed(2);
        return result;
    };
    window.calculateValue = calculateValue;
})();
