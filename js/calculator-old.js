(function ($) {
    $(document).ready(function () {
        var calculateValue = window.calculateValue,
        updateView = function (args) {
            //console.log("update view ran")
            args = args || {};
            args.update = args.update || false;
            var balanceVal, interestVal;
            if (args) {
                switch (args.targ) {
                    case "balance":
                        balanceVal = args.value;
                        break;
                    case "interest":
                        interestVal = args.value;
                        break;
                }
            }
            balanceVal = balanceVal || $bSlider.slider("value");
            interestVal = interestVal || $ciSlider.slider("value");

            if ($ciInput.val() == interestVal && $bInput.val() == balanceVal && !args.update) return; //nothing changed; end function;


            if ($ciInput.val() != interestVal || args.update) {
                //rotate balance dial                
                var opts = $ciSlider.slider("option");
            }

            if (accounting.unformat($bInput.val()) != balanceVal || args.update) {
                //rotate interest dial                
                var opts = $bSlider.slider("option");
            }

            var dg = new DigitCounter();
            var bFrom = { property: parseFloat(accounting.unformat($bInput.val())) }, bTo = { property: balanceVal };
            if (bFrom.property != bTo.property || args.update) {
                $bInput.val(accounting.formatMoney(balanceVal, "", 0, ",", "."));
                $('#balanceValue').text($bInput.val());
            }

            var ciFrom = { property: parseFloat(accounting.unformat($ciInput.val())) }, ciTo = { property: interestVal };
            if (ciFrom.property != ciTo.property || args.update) {
                $ciInput.val(accounting.formatMoney(interestVal, "", 2, ",", ".")+"%");
            }
            var result = calculateValue(balanceVal, interestVal / 12, $balPeriod.text(), currentInterestValue);
            var from = { property: parseFloat(accounting.unformat($totalValue.text())) };
            var to = { property: result };
            if (parseFloat($totalValue.text()) == result) return;

            var options = {
                symbol: "",
                decimal: ".",
                thousand: ",",
                precision: 2,
                format: "%s%v"
            };

            dg.countTo(from, to, $totalValue, function () {
                $totalValue.html(accounting.formatMoney(this.property, options));
            }, function () {
                $totalValue.html(accounting.formatMoney(result, options));
            });
        };
        var $bInput = $('#currentBalance input'), 
            $ciInput = $('#interestRate input');

        var balanceArgs = {
            animate: "fast",
            max: 50000,
            min: 0,
            step: 100,
            value: $bInput.val().replace('$', '').replace(',', ''),
            change: function (evt, ui) {
                updateView({ targ: "balance", value: ui.value, update: false });
            },
            /* slide: function (evt, ui) {
                updateView({ targ: "balance", value: ui.value, update: false });
            }*/
        };

        var interestArgs = {
            animate: "fast",
            max: 40,
            min: parseFloat($(".ir").text()),
            step: 0.01,
            value: $ciInput.val().replace('%', ''),
            change: function (evt, ui) {
                updateView({ targ: "interest", value: ui.value, update: false });
            },
            /* slide: function (evt, ui) {
                updateView({ targ: "interest", value: ui.value, update: false });
            }*/
        };

        var $totalValue = $("#totalValue");
        var is = new InitializeSlider();
            is.setup({
                targ: "#currentBalance .slider",
                args: balanceArgs,
                update: updateView,
                input: "#currentBalance input"
            });
            is.setup({
                targ: "#interestRate .slider",
                args: interestArgs,
                update: updateView,
                input: "#interestRate input"
            });
        var $html = $("html"),
            $bSlider = $("#currentBalance .slider").slider(),
            $ciSlider = $("#interestRate .slider").slider(),
            $bIe,
            $ciIe,
            $balPeriod = $(".bal-period").first(),
            currentInterestValue = $(".ir").first().text();

        updateView({ update: true });


        /*----------------------------------------------*/
        /* Additional functions
        /*----------------------------------------------*/

        // Clear the interval when one of the input fields is selected
        $('#currentBalance input, #interestRate input').click(function(){
            clearInterval(reAnimate);
            $(this).select();
            //console.log("cleared")
        });

        // Set the interval and make the animation start from 0
        var reAnimate = setInterval(function(){
            var startValue = 0;
            $totalValue.html(startValue);
            updateView({ update: true });
        }, 5000);

    });
})(window.jQuery);